package com.rany.mvvmdemo.bean;

/**
 * Description：用于显示页面title 自动展示
 * Created on 2020/6/8
 * Author : 郭
 */
public class TitleBean {
    /**
     * 标题
     */
    private String title;
    //左侧返回键
    private boolean titleLeft;
    //右侧文字
    private String titleRight;
    //右侧图标
    private int titleRightRes;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getTitleRight() {
        return titleRight;
    }

    public void setTitleRight(String titleRight) {
        this.titleRight = titleRight;
    }

    public boolean isTitleLeft() {
        return titleLeft;
    }

    public void setTitleLeft(boolean titleLeft) {
        this.titleLeft = titleLeft;
    }

    public int getTitleRightRes() {
        return titleRightRes;
    }

    public void setTitleRightRes(int titleRightRes) {
        this.titleRightRes = titleRightRes;
    }

    @Override
    public String toString() {
        return "TitleBean{" +
                "title='" + title + '\'' +
                ", titleLeft=" + titleLeft +
                ", titleRight='" + titleRight + '\'' +
                ", titleRightRes=" + titleRightRes +
                '}';
    }
}
