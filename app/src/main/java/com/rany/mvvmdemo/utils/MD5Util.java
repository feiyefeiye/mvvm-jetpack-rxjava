package com.rany.mvvmdemo.utils;

import java.security.MessageDigest;

/**
 * MD5 加密
 */
public class MD5Util {

//    public static String getMD5Encoding(String s) {
//        byte[] input = s.getBytes();
//        String output = null;
//        char[] hexChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
//        try {
//            MessageDigest md = MessageDigest.getInstance("MD5");
//            md.update(input);
//            byte[] tmp = md.digest();
//            char[] str = new char[32];
//            byte b = 0;
//            for (int i = 0; i < 16; i++) {
//                b = tmp[i];
//                str[2 * i] = hexChar[b >>> 4 & 0xf];
//                str[2 * i + 1] = hexChar[b & 0xf];
//            }
//            output = new String(str);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return output;
//    }
    public static String getMD5(String str) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        char[] charArray = str.toCharArray();
        byte[] byteArray = new byte[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            byteArray[i] = (byte) charArray[i];
        }
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }
}
