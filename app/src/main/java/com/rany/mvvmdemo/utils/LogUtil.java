package com.rany.mvvmdemo.utils;

import android.util.Log;

import com.rany.mvvmdemo.BuildConfig;
import com.rany.mvvmdemo.pub.PubConst;


/**
 * log  工具类
 */
public class LogUtil {
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = PubConst.SP_TAG;

    public static void i(String info) {
        if (DEBUG) Log.i(TAG, info);
    }

    public static void d(String info) {
        if (DEBUG) Log.d(TAG, info);
    }

    public static void e(String info) {
        if (DEBUG) Log.e(TAG, info);
    }

    public static void showLog(Object log) {
        if (DEBUG) Log.e(TAG, String.valueOf(log));
    }

    public static void showLog(String tag, Object log) {
        if (DEBUG) Log.e(tag, String.valueOf(log));
    }

    public static void showAll(String info) {
        int LOG_MAXLENGTH = 5000;
        if (DEBUG) {
            int strLength = info.length();
            int start = 0;
            int end = LOG_MAXLENGTH;
            for (int i = 0; i < 100; i++) {
                if (strLength > end) {
                    e(info.substring(start, end));
                    start = end;
                    end = end + LOG_MAXLENGTH;
                } else {
                    e(info.substring(start, strLength));
                    break;
                }
            }
        }
    }
}
