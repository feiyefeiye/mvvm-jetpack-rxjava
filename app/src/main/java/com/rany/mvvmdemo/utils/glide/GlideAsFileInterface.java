package com.rany.mvvmdemo.utils.glide;

public interface GlideAsFileInterface {
    void getPathSuccess(String absolutePath);
    void getPathFail();
}
