package com.rany.mvvmdemo.utils.glide;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RawRes;
import androidx.annotation.WorkerThread;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.io.File;

/**
 * Glide加载图片的辅助接口，APP需要显示图片的地方应该都用该接口来显示，因为可以把token去掉作为cache的key
 */
public class GlideUtils {
    public static void clear(ImageView view) {
        if (view != null) {
            view.setImageDrawable(null);
        }
    }

    @WorkerThread
    public static void clearDiskCache(Context context) {
        Glide.get(context).clearDiskCache();
    }

    /**
     * 计算glide所占用的磁盘空间大小
     * TODO 尚未实现
     *
     * @return
     */
    @WorkerThread
    public static long getCacheSize() {
        long result = 0L;
        try {
        } catch (Exception e) {
        }

        return result;
    }

    /**
     * 加载小图，即在图片url追加thumbnail=small的参数
     *
     * @param context   上下文
     * @param url       原始url，应该是经过了地址替换的
     * @param imageView target
     * @param circle    是否要截圆
     * @param defRes    占位图
     */
    public static void loadSmallImage(Context context, String url, ImageView imageView, boolean circle, int defRes) {
        if (TextUtils.isEmpty(url)) {
            loadNativeImage(context, imageView, circle, defRes);
            return;
        }

        GlideUrl glideUrl = new GlideUrlNoCacheToken.Builder()
                .url(url)
                .appendQueryParameter("thumbnail", "small")
                .build();

        RequestOptions requestOptions = new RequestOptions();
        if (circle) {
            requestOptions = requestOptions.circleCrop();
        }
        if (defRes != 0) {
            requestOptions = requestOptions.placeholder(defRes);
        }

        Glide.with(context).load(glideUrl)
                .apply(requestOptions)
                .into(imageView);
    }

    /**
     * 加载原图
     *
     * @param context   上下文
     * @param url       原始url，应该是经过了地址替换的
     * @param imageView target
     * @param defRes    占位图
     */
    public static void loadLargeImage(Context context, String url, ImageView imageView, boolean circle, int defRes) {
        if (TextUtils.isEmpty(url)) {
            loadNativeImage(context, imageView, circle, defRes);
            return;
        }

        GlideUrl glideUrl = new GlideUrlNoCacheToken.Builder()
                .url(url)
                .build();

        RequestOptions requestOptions = new RequestOptions().placeholder(defRes);
        if (circle) {
            requestOptions = requestOptions.circleCrop();
        }

        Glide.with(context).load(glideUrl)
                .apply(requestOptions)
                .into(imageView);
    }

    /**
     * 加载本地资源
     *
     * @param context   上下文
     * @param imageView target
     * @param circle    是否截圆
     * @param res       本地资源，必须是drawable
     */
    public static void loadNativeImage(Context context, ImageView imageView, boolean circle, int res) {
        if (circle) {
            Glide.with(context)
                    .load(res)
                    .apply(new RequestOptions().circleCrop())
                    .into(imageView);
        } else {
            Glide.with(context)
                    .load(res)
                    .into(imageView);
        }
    }

    /**
     * 加载本地资源
     *
     * @param context   上下文
     * @param imageView target
     * @param circle    是否截圆
     * @param path      存放在本地的图片
     */
    public static void loadNativeImage(Context context, ImageView imageView, boolean circle, String path) {
        if (circle) {
            Glide.with(context)
                    .load(path)
                    .apply(new RequestOptions().circleCrop())
                    .into(imageView);
        } else {
            Glide.with(context)
                    .load(path)
                    .into(imageView);
        }
    }


    public static void getPathfromDiskCache(Context context, String url, @NonNull final GlideAsFileInterface fileInterface) {
        RequestManager mRequestManager = Glide.with(context);
        RequestBuilder<File> mRequestBuilder = mRequestManager.downloadOnly();
        mRequestBuilder.load(url);
        mRequestBuilder.listener(new RequestListener<File>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<File> target, boolean isFirstResource) {
                fileInterface.getPathFail();
                return false;
            }

            @Override
            public boolean onResourceReady(File resource, Object model, Target<File> target, DataSource dataSource, boolean isFirstResource) {
                fileInterface.getPathSuccess(resource.getAbsolutePath());
                return false;
            }
        });
        mRequestBuilder.preload();
    }

    public static void loadGif(Context context, @RawRes @DrawableRes @Nullable Integer resourceId, ImageView imageView) {
            Glide.with(context).load(resourceId).listener(new RequestListener() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                if (resource instanceof GifDrawable) {
                    //加载一次
                    ((GifDrawable)resource).setLoopCount(0);
                }
                return false;
            }

        }).into(imageView);
    }
}

