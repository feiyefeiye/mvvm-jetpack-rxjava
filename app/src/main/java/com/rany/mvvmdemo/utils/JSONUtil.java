package com.rany.mvvmdemo.utils;

import com.google.gson.Gson;
import com.rany.mvvmdemo.application.MyApplication;
import com.rany.mvvmdemo.pub.PubConst;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Locale;

public class JSONUtil {


    /**
     * bean 转 JSONArray
     *
     * @param obj 为空 直接返回null
     * @return
     */
    public static JSONArray beanToJSONArray(Object obj) {
        if (obj == null) return null;
        JSONArray array = new JSONArray();
        array.put(obj);
        return array;
    }

    /**
     * Bean转换为JSONObject
     *
     * @param obj
     * @return
     */
    public static JSONObject beanToJSONObject(Object obj) {
        JSONObject jsonObj = null;
        try {
            Gson gson = new Gson();
            String json_string = gson.toJson(obj);
            jsonObj = new JSONObject(json_string);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    /**
     * Bean转换为String
     *
     * @param obj
     * @return
     */
    public static String beanToJSONString(Object obj) {
        JSONObject jsonObj = null;
        try {
            Gson gson = new Gson();
            String json_string = gson.toJson(obj);
            jsonObj = new JSONObject(json_string);

            return jsonObj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 从List转化为JSONArray
     *
     * @param list
     * @return
     */
    public static JSONArray fromListToJSONArray(List list) {
        try {
            if (list == null || list.size() == 0) return null;
            JSONArray jsonarray = new JSONArray();
            for (int i = 0; i < list.size(); i++)
                jsonarray.put(list.get(i));
            return jsonarray;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 从List转化为JSONArray
     *
     * @param list
     * @return
     */
    public static String fromListToJsonString(List list) {
        try {
            if (list == null || list.size() == 0) return null;
            Gson gson = new Gson();
            String strjson = gson.toJson(list);
            return strjson;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 保存JSONArray格式的字典类文件
     *
     * @param dictName
     */
    public static void updateDict(List list, String dictName) {
        try {
            if (list != null) {
                String filename = String.format(Locale.CHINESE, "%s/%s.%s",
                        MyApplication.getInstance().getFilesDir().toString(), dictName, PubConst.DICT_FILE_EXT);
                String strJson = fromListToJsonString(list);
                if (PubConst.BANNERS.equals(dictName)) { // 如果是banner条 先删除原文件 萧
                    try {
                        File f = new File(filename);
                        if (f != null && f.exists()) {
                            f.delete();
                        }
                    } catch (Exception e) {
                    }
                }
                FileUtil.writeFile(filename, strJson);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
