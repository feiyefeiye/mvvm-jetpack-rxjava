package com.rany.mvvmdemo.utils.glide;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.load.model.GlideUrl;

import java.net.URLDecoder;

/**
 * 以忽略 query part 的 Url 来计算 cache-key, 供Glide库使用.
 *
 * @author lihanguang
 * @date 2016/5/18 17:39:57
 */
public class GlideUrlNoCacheToken extends GlideUrl {
    private static final String TAG = "GlideUrlNoCacheToken";

    Uri.Builder mUriBuilder;

    private String mCacheUrl;

    // 不需要默认的 GlideUrlNoCacheToken
    /*public GlideUrlNoCacheToken() {
        this(new Builder()
                .url("http://DEFAULT_DOMAIN:9090")
        );
    }*/

    private GlideUrlNoCacheToken(Builder builder) {
        super(toUrlStr(builder.mUriBuilder.build()));
        mUriBuilder = builder.mUriBuilder;
        mCacheUrl = clearQueryParam(super.getCacheKey(), "token"); // cache key 不包含 "token=XXX"
    }

    @Override
    public String getCacheKey() {
        return mCacheUrl;
    }

    @Override
    public String toString() {
        return super.getCacheKey();
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    private static String toUrlStr(Uri uri) {
        String encodedStr = uri.toString();
        try {
            return URLDecoder.decode(encodedStr, "UTF-8");
        } catch (Exception e) {
            String errorMsg = "Fail to decode url: " + String.valueOf(encodedStr);
            Log.e(TAG, errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }
    }

    /**
     * 删除 url 中的特定查询参数.
     * <p>
     * 由于 url 的参数 只存在几种种模式, 假设下面我们要删除 "key=value":
     * 1) eim.mygzb.com?key=value // 处于头部
     * 2) eim.mygzb.com?key=value&key2=value2&...&key_N=value_N // 处于头部,但后面紧跟其他参数
     * 3) eim.mygzb.com?key2=value2&key=value&...&key_N=value_N // 处于中间某个文字
     * 4) eim.mygzb.com?key2=value2&...&key_N=value_N&key=value // 处于尾部
     * <p>
     * 所以我们只需先将匹配到 "&key=value" 的子串删掉(对应 3, 4),
     * 然后再将将匹配到 "key=value" 或 "key=value&" 的子串删除(对应 1, 2).
     */
    public static String clearQueryParam(String url, String key) {
        String result = url;
        int idx = url.indexOf("?");
        if (idx != -1) {
            String basePart = url.substring(0, idx);
            String queryPart = url.substring(idx + 1);
            if (TextUtils.isEmpty(queryPart)) { // last char is '?'
                result = basePart;
            } else {
                String regularExpression1 = "[&]" + key + ".*?(?=&|\\?|$)";
                String regularExpression2 = key + ".*?(?=&|\\?|$)[&]{0,1}";
                queryPart = queryPart.replaceAll(regularExpression1, "").replaceAll(regularExpression2, "");
                if (TextUtils.isEmpty(queryPart)) {
                    result = basePart;
                } else {
                    result = basePart + '?' + queryPart;
                }
            }
        }
        return result;
    }

    public static class Builder {
        private Uri.Builder mUriBuilder;

        public Builder() {
            mUriBuilder = new Uri.Builder();
        }

        private Builder(GlideUrlNoCacheToken glideUrl) {
            mUriBuilder = glideUrl.mUriBuilder;
        }

        public Builder url(String url) {
            Uri newUri = Uri.parse(url);
            mUriBuilder
                    .scheme(newUri.getScheme())
                    .authority(newUri.getAuthority())
                    .query(newUri.getQuery())
                    .fragment(newUri.getFragment())
                    .path(newUri.getPath());
            return this;
        }

        public Builder appendQueryParameter(String key, String value) {
            mUriBuilder.appendQueryParameter(key, value).build();
            return this;
        }

        public GlideUrl build() {
            return new GlideUrlNoCacheToken(this);
        }
    }
}
