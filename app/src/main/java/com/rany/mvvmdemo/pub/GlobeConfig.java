package com.rany.mvvmdemo.pub;

import android.content.Context;

import com.rany.mvvmdemo.ui.bean.UserBean;


/**
 * Description：封装存储数据
 * Created on 2017/9/28
 * Author : 郭
 */

public class GlobeConfig {
    /**
     * 是否是打包版本 默认debug
     */
    public static final boolean RELEASE = false;
    /**
     * 工程包名：SD卡保存路径时使用
     */
    public static final String PACKAGE_NAME = "com.pgy.langooo";
    private static UserBean userBean;
    private static int userid;
    /**
     * 登录手机号码
     */
    private static String mobile;


    /**
     * langooo设备唯一标识
     */
    private static String uuid;


    /**
     * // 初始化所有全局变量
     *
     * @param context
     */
    public static void init(Context context) {
        userBean = DataModule.getInstance().getLoginUserInfo();
        userid = DataModule.getInstance().getLoginedUserId();
        mobile = DataModule.getInstance().getMobile();
        uuid = DataModule.getInstance().getUUID(context);
    }


    /**
     * 保存用户信息
     *
     * @param userBean
     */
    public static void saveUsetInfo(UserBean userBean) {
        DataModule.getInstance().saveLoginedUserInfo(userBean);
    }


    /**
     * 拿到Userid
     *
     * @return
     */
    public static int getUserId() {
        DataModule instance = DataModule.getInstance();
        if (instance != null) {
            return instance.getLoginedUserId();
        } else {
            return 0;
        }

    }

    public static UserBean getUserInfo() {
        return DataModule.getInstance().getLoginUserInfo();
    }

    /**
     * 退出登录
     */
    public static void logout() {
        userBean = null;
        userid = 0;
        mobile = null;
        DataModule.getInstance().logout();
    }




}
