package com.rany.mvvmdemo.pub;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.rany.mvvmdemo.application.MyApplication;
import com.rany.mvvmdemo.ui.bean.UserBean;
import com.rany.mvvmdemo.utils.AppUtil;
import com.rany.mvvmdemo.utils.FileUtil;
import com.rany.mvvmdemo.utils.JSONUtil;
import com.rany.mvvmdemo.utils.SpUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 获取修改保存到本地数据的方法类
 */
public class DataModule {
    private static DataModule instance = null;

    public static DataModule getInstance() {
        if (instance == null) {
            instance = new DataModule();
        }

        return instance;
    }

    /**
     * 获取已登录用户
     */
    public UserBean getLoginUserInfo() {
        try {
            UserBean pb = (UserBean) SpUtils.deSerialize(PubConst.USER + getLoginedUserId());
            return pb;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 直接保存
     */
    public void saveList(List list, String keyword) {
        if (list == null || list.size() == 0) return;
        JSONUtil.updateDict(list, keyword);
    }

    /**
     * 保存已登录用户信息
     */
    public void saveLoginedUserInfo(UserBean user) {
        saveLoginUserInfo(user);
    }

    /**
     * 保存已登录用户
     */
    private void saveLoginUserInfo(UserBean user) {
        try {
            if (user == null) return;
            //单独保存userid  方便使用
            SpUtils.setInt(PubConst.KEY_LOGINED_USERID, user.getUid());
            SpUtils.serialize(user, PubConst.USER + user.getUid());
            SpUtils.setString(PubConst.KEY_MOBILE, user.getTelephone());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 退出登录，清除登录记录
     */
    public void logout() {
        removeLoginedUserInfo();
    }


    public void removeLoginedUserInfo() {
        SpUtils.removeData(PubConst.USER + getLoginedUserId());
        SpUtils.setString(PubConst.KEY_MOBILE, "");
        SpUtils.setInt(PubConst.KEY_LOGINED_USERID, 0);
    }


    public static float getBMI(int height, float weight) {
        if (height == 0 || weight == 0) return 0;
        float bmi = weight / (((float) height) / 100 * 2);
        return bmi;
    }


    /**
     * 字典文件存储路径
     *
     * @return
     */
    public static String getDictFilePath() {
        return MyApplication.getInstance().getFilesDir().toString();
    }

    /**
     * 字典： 调用方法：List<GlucometerBean> a =
     * DataModule.loadDict(MyProfile.DICT_GLUCOMETERS, GlucometerBean.class);
     */
    public static <T> List<T> loadDict(String dictName, Class<T> classOfT) {

        String filename = String.format(Locale.CHINESE, "%s/%s.%s", getDictFilePath(), dictName, PubConst.DICT_FILE_EXT);
        String json_string = FileUtil.readFile(filename);
        if (json_string == null) return null;

        try {
            Gson gson = new Gson();
            List<T> dataList = new ArrayList<T>();
            JSONArray ja = new JSONArray(json_string);
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);

                T gb = gson.fromJson(jo.toString(), classOfT);
                if (gb != null) dataList.add(gb);
            }

            return dataList;

        } catch (Exception e) {
        }

        return null;
    }


    /**
     * 取得登录用户ID
     *
     * @return
     */
    public int getLoginedUserId() {
        return SpUtils.getInt(PubConst.KEY_LOGINED_USERID, 0);
    }

    /**
     * /**
     * 获取当前设备的唯一识别码 如无唯一识别码，则新生成一个
     *
     * @return
     */
    public String getUUID(Context context) {
        String uuid = SpUtils.getString(PubConst.KEY_UUID, "");
        if (TextUtils.isEmpty(uuid)) {
            uuid = AppUtil.getUUID(context);
            SpUtils.setString(PubConst.KEY_UUID, uuid);
        }
        return uuid;
    }


    /**
     * 获取首次启动标记
     *
     * @param context
     * @return
     */
    public int getIsStarted(Context context) {
        return SpUtils.getInt(PubConst.KEY_ISSTARTED + AppUtil.getVersionCode(context), 1);
    }

    /**
     * 保存首次访问标记
     */
    public void saveIsStarted(Context context) {
        SpUtils.setInt(PubConst.KEY_ISSTARTED + AppUtil.getVersionCode(context), 2);
    }

    /**
     * 获取保存的手机号码
     *
     * @return
     */
    public String getMobile() {
        return SpUtils.getString(PubConst.KEY_MOBILE, "");
    }


}
