package com.rany.mvvmdemo.pub;

/**
 * Description：常量
 * Created on 2020/09/09
 * Author : 郭
 */

public class PubConst {
    /**
     * 用于传值
     */
    public static final String DATA = "data";
    public static final String KEY = "key";
    public static final String FALG = "falg";
    public static final String URL = "url";
    public static final String TYPE = "type";
    public static final String POSITION = "position";
    public static final String BEAN = "bean";
    public static final String DESCRIBE = "des";
    public static final String IMAGE = "image";
    public static final String TITLE = "title";
    public static final String CONTENT = "content";
    public static final String ID = "id";
    //用于定义包名等等 sp名字 数据名字 等等
    public static final String SP_TAG = "langooo";

    public static final String ISFIRST = "isfirst";
    public static final String UM_DEVICETOKEN = "um_devicetoken";
    public static final String VERSION_CODE = "versionCode";
    public static final String VERSION_NAME = "versionName";

    public static final String USER = "user";
    public static final String KEY_LOGINED_USERID = "userid";
    public static final String KEY_LOGINED_USERTYPE = "userType";

    public static final String DICT_FILE_EXT = "dic";
    public static final String COLOR = "color";

    public static final String BANNERS = "banners";
    public static final String NIM_USER = "nim_user";
    public static final String NIM_ACCOUNT = "nim_account";

    public static final String KEY_UUID = "key_uuid";
    public static final String KEY_ISSTARTED = "isstarted";// 是否已经启动过 0 否 1 是
    public static final String KEY_MOBILE = "key_mobile";// 手机号码
    public static final String TRA_CH = "zh";
    public static final String TRA_EN = "en";
    public static final String LIST = "list";
    public static final String UPLOAD_VIDEO = "video";
    public static final String UPLOAD_AUDIO = "audio";
    public static final String UPLOAD_PICTURE_LIST = "pictures";
    public static final String START_APP = "start_app";



}
