package com.rany.mvvmdemo.request;
import com.rany.mvvmdemo.http.base.BaseRequestBean;

/**
 * Description：一般不用base进行请求 需要commonrequest  应对突然增加的参数
 * Created on 2020/6/9
 * Author : 郭
 */
public class CommonRequestBean extends BaseRequestBean {

}
