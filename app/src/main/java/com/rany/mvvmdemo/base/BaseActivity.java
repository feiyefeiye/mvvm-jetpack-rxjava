package com.rany.mvvmdemo.base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.jaeger.library.StatusBarUtil;
import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.application.MyApplication;
import com.rany.mvvmdemo.http.base.BaseViewModel;
import com.rany.mvvmdemo.http.bean.DialogBean;
import com.rany.mvvmdemo.http.loading.LoadingDialog;
import com.rany.mvvmdemo.utils.ActivityUtil;
import com.rany.mvvmdemo.utils.ResLoaderUtils;

import java.lang.reflect.ParameterizedType;

/**
 * Description：BaseActivity基类，所有的activity均需继承
 * Created on 2020/9/15
 * Author : 郭
 */

public abstract class BaseActivity<VM extends BaseViewModel, DB extends ViewDataBinding> extends AppCompatActivity {

    protected VM mViewModel;
    protected DB mDataBinding;
    protected Context mContext;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setWindow();
        ActivityUtil.getInstance().addActivity(this);
        int layoutId = getLayoutId();
        setContentView(layoutId);
        mDataBinding = initDataBinding(layoutId);
        mDataBinding.setLifecycleOwner(this);
        setStatusBar();
        //获得泛型参数的实际类型
        initViewModel();
        initObserve();
        initView();
        initData();
        initListener();
    }

    /**
     * 设置状态栏颜色
     */
    protected void setStatusBar() {
        StatusBarUtil.setColorNoTranslucent(this, ResLoaderUtils.getColor(R.color.statusbar_color));
        StatusBarUtil.setDarkMode(this);
    }


    /**
     * 当前viewmoudel 观察viewmodel  用于设置title
     */
    protected void initViewModel() {
        Class<VM> vmClass = (Class<VM>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        mViewModel = new ViewModelProvider(this).get(vmClass);
//        mViewModel =   new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(MyApplication.getInstance())).get(vmClass);
        getLifecycle().addObserver(mViewModel);
    }

    protected void setWindow() {
    }


    /**
     * 初始化要加载的布局资源ID
     */
    protected abstract int getLayoutId();


    /**
     * 初始化DataBinding
     */
    protected DB initDataBinding(@LayoutRes int layoutId) {
        return DataBindingUtil.setContentView(this, layoutId);
    }

    /**
     * 初始化视图
     */
    protected abstract void initView();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 初始化监听
     */
    protected void initListener() {
    }


    /**
     * 监听当前ViewModel中 showDialog和error的值
     */
    private void initObserve() {
        if (mViewModel == null) return;
        mViewModel.getShowDialog(this, new Observer<DialogBean>() {
            @Override
            public void onChanged(DialogBean bean) {
                if (bean.isShow()) {
                    showDialog(bean.getMsg());
                } else {
                    dismissDialog();
                }
            }
        });
        mViewModel.getError(this, new Observer<Object>() {
            @Override
            public void onChanged(Object obj) {
                showError(obj);
            }
        });
    }

    /**
     * ViewModel层发生了错误
     */
    protected abstract void showError(Object obj);

    /**
     * 显示用户等待框
     *
     * @param msg 提示信息
     */
    protected void showDialog(String msg) {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.setLoadingMsg(msg);
        } else {
            loadingDialog = new LoadingDialog(mContext);
            loadingDialog.setLoadingMsg(msg);
            loadingDialog.show();
        }
    }

    /**
     * 隐藏等待框
     */
    protected void dismissDialog() {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDataBinding != null) {
            mDataBinding.unbind();
        }
        ActivityUtil.getInstance().removeActivity(this);
    }

    /**
     * 左侧按钮点击事件
     *
     * @param view
     */
    public void onTitleLeftClickListener(View view) {
        if (view != null) {
            this.finish();
        }

    }

    /**
     * 右侧文字点击事件
     *
     * @param view
     */
    public void onTitleRightTvClickListener(View view) {

    }

    /**
     * 右侧icon点击事件
     *
     * @param view
     */
    public void onTitleRightIconClickListener(View view) {

    }
}
