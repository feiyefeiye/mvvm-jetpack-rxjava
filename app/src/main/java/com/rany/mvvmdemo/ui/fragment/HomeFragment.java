package com.rany.mvvmdemo.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseLazyFragment;
import com.rany.mvvmdemo.databinding.HomeFragmentBinding;
import com.rany.mvvmdemo.ui.activity.MainActivity;
import com.rany.mvvmdemo.ui.vm.HomeModel;
import com.rany.mvvmdemo.utils.LogUtil;

/**
 * Description：
 * Created on 2020/9/16
 * Author : 郭
 */
public class HomeFragment extends BaseLazyFragment<HomeModel, HomeFragmentBinding> {

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutId() {
        return R.layout.home_fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mDataBinding.setVm(mViewModel);
        LogUtil.showAll("===11111===home");

    }

    @Override
    protected void initListener() {
        mDataBinding.tvHome.setOnClickListener(view -> startActivity(new Intent().setClass(activity, MainActivity.class)));
    }

    @Override
    protected void showError(Object obj) {

    }

    @Override
    protected void lazyLoad() {
        LogUtil.showAll("===2222===home");
    }
}
