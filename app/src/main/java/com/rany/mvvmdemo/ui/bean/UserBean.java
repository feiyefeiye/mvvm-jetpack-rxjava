package com.rany.mvvmdemo.ui.bean;

/**
 * Description：
 * Created on 2020/6/8
 * Author : 郭
 */
public class UserBean {
    private  int  uid;
    private String telephone;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
