package com.rany.mvvmdemo.ui.activity;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseActivity;
import com.rany.mvvmdemo.databinding.ActivityMainBinding;
import com.rany.mvvmdemo.databinding.ActivityMainTabBinding;
import com.rany.mvvmdemo.ui.fragment.CourseFragment;
import com.rany.mvvmdemo.ui.fragment.HomeFragment;
import com.rany.mvvmdemo.ui.fragment.MineFragment;
import com.rany.mvvmdemo.ui.vm.MainTabMudel;

import java.util.ArrayList;
import java.util.List;

import static androidx.navigation.ui.NavigationUI.setupActionBarWithNavController;

/**
 * Description：首页tab
 * Created on 2020/9/16
 * Author : 郭
 */
public class MainTabActivity extends BaseActivity<MainTabMudel, ActivityMainTabBinding> {

    /**
     * fm集合
     */
    private List<Fragment> fragmentList = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main_tab;
    }

    @Override
    protected void initView() {


    }

    @Override
    protected void initListener() {
        super.initListener();
        mDataBinding.mainViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mDataBinding.bottomNav.getMenu().getItem(position).setChecked(true);
            }

        });
        mDataBinding.bottomNav.setOnNavigationItemSelectedListener(item -> scrollToFragment(item));
    }

    /**
     * 滚动到不同的fm
     *
     * @param item
     * @return
     */
    private boolean scrollToFragment(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.homeFragment:
                mDataBinding.mainViewPager.setCurrentItem(0, true);
                break;
            case R.id.courseFragment:
                mDataBinding.mainViewPager.setCurrentItem(1, true);
                break;
            case R.id.mineFragment:
                mDataBinding.mainViewPager.setCurrentItem(2, true);
                break;
            default:
                mDataBinding.mainViewPager.setCurrentItem(0, true);
                break;
        }
        return false;
    }

    @Override
    protected void initData() {

        //设置选中时的效果
        mDataBinding.bottomNav.setItemTextAppearanceActive(R.style.main_bottom_style);
        //设置未选中的效果
        mDataBinding.bottomNav.setItemTextAppearanceInactive(R.style.main_bottom_uncheck_style);
        mDataBinding.bottomNav.setItemIconTintList(null);

        fragmentList.add(HomeFragment.newInstance());
        fragmentList.add(CourseFragment.newInstance());
        fragmentList.add(MineFragment.newInstance());
        mDataBinding.mainViewPager.setOffscreenPageLimit(1);
        mDataBinding.mainViewPager.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getItemCount() {
                return fragmentList.size();
            }
        });


    }

    @Override
    protected void showError(Object obj) {

    }
}