package com.rany.mvvmdemo.ui.activity;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseActivity;
import com.rany.mvvmdemo.databinding.ActivityFragemntContentBinding;
import com.rany.mvvmdemo.ui.vm.FragmentContentViewModel;

/**
 * Description：
 * Created on 2020/9/17
 * Author : 郭
 */
public class FragmentContentActivity extends BaseActivity<FragmentContentViewModel, ActivityFragemntContentBinding> {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_fragemnt_content;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void showError(Object obj) {

    }

    @Override
    public boolean onNavigateUp() {
        return super.onNavigateUp();
    }
}
