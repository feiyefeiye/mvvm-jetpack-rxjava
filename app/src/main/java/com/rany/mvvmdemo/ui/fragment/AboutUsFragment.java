package com.rany.mvvmdemo.ui.fragment;


import android.os.Bundle;
import android.view.View;

import androidx.navigation.Navigation;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseFragment;
import com.rany.mvvmdemo.databinding.FragmentAboutUsBinding;
import com.rany.mvvmdemo.ui.vm.AboutUsViewMode;
import com.rany.mvvmdemo.utils.LogUtil;
import com.rany.mvvmdemo.utils.ToastUtil;

public class AboutUsFragment extends BaseFragment<AboutUsViewMode, FragmentAboutUsBinding> {



    @Override
    protected int getLayoutId() {
        return R.layout.fragment_about_us;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

        Bundle arguments = getArguments();
        int argument = arguments.getInt("argument");
        LogUtil.showAll(String.valueOf(argument));
    }

    @Override
    protected void initData() {

    }
    @Override
    protected void initListener() {
        mDataBinding.tvHello.setOnClickListener(v -> ToastUtil.showToast(mDataBinding.tvHello.getText().toString().trim()));
    }


    @Override
    protected void showError(Object obj) {

    }


}