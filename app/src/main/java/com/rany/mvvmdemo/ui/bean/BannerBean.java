package com.rany.mvvmdemo.ui.bean;

/**
 * Description：
 * Created on 2020/6/9
 * Author : 郭
 */
public class BannerBean {
    private int id;
    private String title ;
    private String urlImg;
    private String link;
    private int isHot;
    private int isShare;
    private int topicType;
    private int topicId;
    private String describe;
    private String imgUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public int getIsShare() {
        return isShare;
    }

    public void setIsShare(int isShare) {
        this.isShare = isShare;
    }

    public int getTopicType() {
        return topicType;
    }

    public void setTopicType(int topicType) {
        this.topicType = topicType;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public String toString() {
        return "BannerBean{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", urlImg='" + urlImg + '\'' +
                ", link='" + link + '\'' +
                ", isHot=" + isHot +
                ", isShare=" + isShare +
                ", topicType=" + topicType +
                ", topicId=" + topicId +
                ", describe='" + describe + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}
