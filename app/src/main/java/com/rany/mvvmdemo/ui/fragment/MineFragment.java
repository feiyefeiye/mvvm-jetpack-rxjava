package com.rany.mvvmdemo.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseLazyFragment;
import com.rany.mvvmdemo.databinding.MineFragmentBinding;
import com.rany.mvvmdemo.ui.activity.FragmentContentActivity;
import com.rany.mvvmdemo.ui.vm.MineModel;
import com.rany.mvvmdemo.utils.LogUtil;

/**
 * Description：
 * Created on 2020/9/16
 * Author : 郭
 */
public class MineFragment extends BaseLazyFragment<MineModel, MineFragmentBinding> {

    public static MineFragment newInstance() {
        Bundle args = new Bundle();
        MineFragment fragment = new MineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.mine_fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
    }

    @Override
    protected void initData() {
        mDataBinding.setVm(mViewModel);

    }

    @Override
    protected void initListener() {
        mDataBinding.btnTofm.setOnClickListener(view -> startActivity(new Intent().setClass(activity, FragmentContentActivity.class)));
    }

    @Override
    protected void showError(Object obj) {

    }

    @Override
    protected void lazyLoad() {
        LogUtil.showAll("==222222====mine");
    }
}
