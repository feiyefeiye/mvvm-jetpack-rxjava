package com.rany.mvvmdemo.ui.activity;

import android.content.Intent;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseActivity;
import com.rany.mvvmdemo.databinding.ActivityLaunchrBinding;
import com.rany.mvvmdemo.ui.vm.LauncherModel;

public class LaunchrActivity extends BaseActivity<LauncherModel, ActivityLaunchrBinding> {


    @Override
    protected int getLayoutId() {
        return R.layout.activity_launchr;
    }

    @Override
    protected void initView() {
        startActivity(new Intent().setClass(this, MainTabActivity.class));
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void showError(Object obj) {

    }
}