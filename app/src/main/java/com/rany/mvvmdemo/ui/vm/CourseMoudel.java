package com.rany.mvvmdemo.ui.vm;

import android.app.Application;

import androidx.annotation.NonNull;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.http.base.BaseViewModel;
import com.rany.mvvmdemo.utils.ResLoaderUtils;

/**
 * Description：
 * Created on 2020/9/16
 * Author : 郭
 */
public class CourseMoudel extends BaseViewModel {
    public CourseMoudel(@NonNull Application application) {
        super(application);

    }

    @Override
    public void setTitle() {
        title.setTitleBar(false, ResLoaderUtils.getString(R.string.tab_course));
    }
}
