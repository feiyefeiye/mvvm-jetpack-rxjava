package com.rany.mvvmdemo.ui.fragment;

import android.os.Bundle;
import android.view.View;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseLazyFragment;
import com.rany.mvvmdemo.databinding.CourseFragmentBinding;
import com.rany.mvvmdemo.ui.vm.CourseMoudel;
import com.rany.mvvmdemo.utils.LogUtil;


import io.flutter.embedding.android.FlutterActivity;

/**
 * Description：
 * Created on 2020/9/16
 * Author : 郭
 */
public class CourseFragment extends BaseLazyFragment<CourseMoudel, CourseFragmentBinding> {
    public static CourseFragment newInstance() {
        
        Bundle args = new Bundle();
        
        CourseFragment fragment = new CourseFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutId() {
        return R.layout.course_fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mDataBinding.setVm(mViewModel);

    }

    @Override
    protected void initListener() {
        mDataBinding.button.setOnClickListener(view ->   startActivity(FlutterActivity.withNewEngine().initialRoute("rount_1").build(mContext)));

    }

    @Override
    protected void showError(Object obj) {

    }

    @Override
    protected void lazyLoad() {
        LogUtil.showAll("===22222===course");
    }
}
