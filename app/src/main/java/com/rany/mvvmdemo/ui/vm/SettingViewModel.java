package com.rany.mvvmdemo.ui.vm;

import android.app.Application;

import androidx.annotation.NonNull;

import com.rany.mvvmdemo.http.base.BaseViewModel;

/**
 * Description：
 * Created on 2020/9/17
 * Author : 郭
 */
public class SettingViewModel extends BaseViewModel {

    public SettingViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void setTitle() {
        title.setTitleBar(true,"设置");
    }
}
