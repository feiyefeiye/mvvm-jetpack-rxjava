package com.rany.mvvmdemo.ui.activity;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseActivity;
import com.rany.mvvmdemo.databinding.ActivityMainBinding;
import com.rany.mvvmdemo.ui.bean.BannerBean;
import com.rany.mvvmdemo.ui.vm.MainModel;
import com.rany.mvvmdemo.utils.ToastUtil;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;

/**
 * Description：首页
 * Created on 2020/9/15
 * Author : 郭
 */
public class MainActivity extends BaseActivity<MainModel, ActivityMainBinding> implements OnRefreshLoadMoreListener {



    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    protected void initView() {


    }

    @Override
    protected void initListener() {
        super.initListener();
        mDataBinding.refreshLayout.setOnRefreshLoadMoreListener(this);
    }



    @Override
    public void onTitleRightIconClickListener(View view) {
        super.onTitleRightIconClickListener(view);
        ToastUtil.showToast("点击了icon");
    }

    @Override
    public void onTitleRightTvClickListener(View view) {
        super.onTitleRightTvClickListener(view);
        ToastUtil.showToast("点击了文字");
    }

    @Override
    protected void initData() {
        mDataBinding.setViewmodel(mViewModel);
        mViewModel.getBannerList().observe(this, new Observer<BannerBean>() {
            @Override
            public void onChanged(BannerBean bannerBean) {
                mDataBinding.refreshLayout.finishRefresh();
                mDataBinding.refreshLayout.finishLoadMore();
            }
        });
//        mViewModel.getInfoList().observe(this, new Observer<List<UserBean>>() {
//            @Override
//            public void onChanged(List<UserBean> userBeans) {
//
//            }
//        });

    }


    @Override
    protected void showError(Object obj) {

    }


    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        mViewModel.onLoadMore();

    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mViewModel.clickBut();
    }
}