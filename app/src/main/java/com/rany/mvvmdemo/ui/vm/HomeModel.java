package com.rany.mvvmdemo.ui.vm;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.http.base.BaseViewModel;

/**
 * Description：
 * Created on 2020/9/16
 * Author : 郭
 */
public class HomeModel extends BaseViewModel {
    private Context context;

    public HomeModel(@NonNull Application application) {
        super(application);
        context = application.getApplicationContext();
    }

    @Override
    public void setTitle() {
        title.setTitleBar(false, context.getString(R.string.tab_home));
    }
}
