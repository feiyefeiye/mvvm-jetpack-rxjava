package com.rany.mvvmdemo.ui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.navigation.Navigation;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.base.BaseFragment;
import com.rany.mvvmdemo.databinding.FragmentSettingBinding;
import com.rany.mvvmdemo.ui.vm.SettingViewModel;

/**
 * Description：
 * Created on 2020/9/17
 * Author : 郭
 */
public class SettingFragment extends BaseFragment<SettingViewModel, FragmentSettingBinding> {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_setting;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mDataBinding.setVm(mViewModel);
    }

    @Override
    protected void initListener() {
        super.initListener();
        mDataBinding.tvToFm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("argument", 100);
                Navigation.findNavController(getView()).navigate(R.id.action_settingFragment_to_aboutUsFragment, bundle);

            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void showError(Object obj) {

    }
}
