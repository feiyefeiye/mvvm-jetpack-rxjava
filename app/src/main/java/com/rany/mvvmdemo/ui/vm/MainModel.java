package com.rany.mvvmdemo.ui.vm;


import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.Navigation;

import com.rany.mvvmdemo.http.base.BaseViewModel;
import com.rany.mvvmdemo.http.manager.SubscriberManger;
import com.rany.mvvmdemo.request.CommonRequestBean;
import com.rany.mvvmdemo.ui.bean.BannerBean;
import com.rany.mvvmdemo.ui.bean.UserBean;
import com.rany.mvvmdemo.utils.LogUtil;

import java.io.IOException;
import java.util.List;


/**
 * Description：
 * Created on 2020/6/8
 * Author : 郭
 */
public class MainModel extends BaseViewModel {

    private  int page = 1;
    /**
     * 当数据请求成功回调
     */
    public MutableLiveData<BannerBean> banner;

    public MutableLiveData<List<UserBean>> userList;

    public MainModel(@NonNull Application application) {
        super(application);
    }

    public void clickBut() {
        page = 1;
        requestImg();
    }

    @Override
    public void setTitle() {
        title.setTitleBar(true, "网络请求","分享");
    }


    /**
     * 发起请求
     */
    public void requestImg() {
        LogUtil.showAll("======"+ page);
        showDialog.setValue(true);
        CommonRequestBean body = new CommonRequestBean();
        addDisposable(manager.requsetStartUpScreen(body), new SubscriberManger<BannerBean>() {
            @Override
            public void onSuccess(BannerBean result, String msg) throws IOException {
                banner.postValue(result);
                page++;
            }

            @Override
            public void onFailure(int code, String msg) throws IOException {
                error.postValue(msg);
            }

            @Override
            public void onFinish() {
                showDialog.setValue(false);
            }
        });

    }


    public MutableLiveData<BannerBean> getBannerList() {
        if (banner == null) {
            banner = new MutableLiveData<>();
        }
        return banner;
    }

    public MutableLiveData<List<UserBean>> getInfoList() {
        if (userList == null) {
            userList = new MutableLiveData<>();
        }
        return userList;
    }

    @Override
    public void onLoadMore() {
        super.onLoadMore();
        requestImg();
    }
}
