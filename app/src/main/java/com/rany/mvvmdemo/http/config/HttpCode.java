package com.rany.mvvmdemo.http.config;

/**
 * Description：错误码
 * Created on 2020/9/10
 * Author : 郭
 */
public class HttpCode {
    /**
     * 成功
     */
    public static final int HTTP_SUCCSS = 200;


    /**
     * 客户端自定义异常
     */
    public static final int HTTP_CLIENT_ERROR = -1;

}
