package com.rany.mvvmdemo.http.manager;

import android.text.TextUtils;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.http.base.BaseResponseBean;
import com.rany.mvvmdemo.http.config.HttpCode;
import com.rany.mvvmdemo.utils.ResLoaderUtils;
import com.rany.mvvmdemo.utils.StringUtil;
import com.rany.mvvmdemo.utils.ToastUtil;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

/**
 * Description：封装loading
 * Created on 2018/6/28 0028
 * Author : 郭
 */
public abstract class SubscriberManger<T> extends DisposableObserver<BaseResponseBean<T>> {

    private static final String TAG = SubscriberManger.class.getSimpleName();

    private String message;

    public SubscriberManger() {
    }

    @Override
    public void onNext(BaseResponseBean<T> t) {
        //200  代表成功
        if (Integer.valueOf(t.getCode()) == HttpCode.HTTP_SUCCSS) {
            try {
                onSuccess(t.getResult(), StringUtil.StrTrim(t.getMessage()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else { //服务器返回错误码
            if (!TextUtils.isEmpty(t.getMessage())) {
                try {
                    onFailure(StringUtil.StrTrimInt(t.getCode()), StringUtil.StrTrim(t.getMessage()));
                    ToastUtil.showToast(t.getMessage());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            } else {
                onError(new RuntimeException());
            }
        }
    }

    //成功回调
    public abstract void onSuccess(T result, String msg) throws IOException;

    //失败回调
    public abstract void onFailure(int code, String msg) throws IOException;

    //完成请求回调
    public void onFinish() {

    }


    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException || e instanceof ConnectException || e instanceof UnknownHostException) {
            message = e.getMessage();
        } else {
            message = e.getMessage();
            if ("timeout".equals(message)) {
                message = ResLoaderUtils.getString(R.string.timeout_error);
            }
        }
        try {
            onFailure(HttpCode.HTTP_CLIENT_ERROR, message);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }

    @Override
    public void onComplete() {
            onFinish();
    }

}
