package com.rany.mvvmdemo.http.api;


import com.rany.mvvmdemo.pub.GlobeConfig;

/**
 * Description：管理请求接口
 * Created on 2020/6/8
 * Author : 郭
 */
public class WAPI {

    /**
     * 测试环境 baseurl
     */
    public static final String TEST_URL = "http://test.api.langooo.com";
    /**
     * 生产环境 baseUrl
     */
    public static final String ONLINE_URL = "http://api.langooo.com";
//    public static final String ONLINE_URL = "http://bete.api.langooo.com";

    /**
     * 生产环境h5 baseyrl
     */
    public static final String ONLIN_H5 = GlobeConfig.RELEASE ? "http://webview.langooo.com" : "http://test.webview.langooo.com";

    /**
     * 生产环境 活动url
     */
    public static final String ONLIN_ACTIVE_H5 = "http://act.langooo.com";


    /**
     * 英式发音
     */
    public static final String ENGLISH_VOICE_UK = "http://dict.youdao.com/dictvoice?type=1&audio=";
    /**
     * 美式发音
     */
    public static final String ENGLISH_VOICE_US = "http://dict.youdao.com/dictvoice?type=2&audio=";


    public static final String URL_UPLOAD_FILE = "file/uploadFile";

    //开屏图
    public static final String URL_START_UP_SCREEN = "/openScreen/getStartUpScreen";



    //登录时的加密
    static byte[] keyBytes = {0x11, 0x22, 0x4F, 0x58, (byte) 0x88, 0x10,
            0x40, 0x38, 0x28, 0x25, 0x79, 0x51, (byte) 0xCB, (byte) 0xDD,
            0x55, 0x66, 0x77, 0x29, 0x74, (byte) 0x98, 0x30, 0x40, 0x36,
            (byte) 0xE2};


}
