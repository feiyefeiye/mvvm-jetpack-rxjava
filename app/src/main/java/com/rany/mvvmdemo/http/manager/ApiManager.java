package com.rany.mvvmdemo.http.manager;

import com.rany.mvvmdemo.http.api.WAPI;
import com.rany.mvvmdemo.http.base.BaseResponseBean;
import com.rany.mvvmdemo.request.CommonRequestBean;
import com.rany.mvvmdemo.ui.bean.BannerBean;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Description：管理请求接口
 * Created on 2018/7/3
 * Author : 郭
 */
public interface ApiManager<T> {

    //请求开屏图
    @POST(WAPI.URL_START_UP_SCREEN)
    Observable<BaseResponseBean<BannerBean>> requsetStartUpScreen(@Body CommonRequestBean body);

}
