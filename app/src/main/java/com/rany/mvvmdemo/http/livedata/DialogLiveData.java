package com.rany.mvvmdemo.http.livedata;

import androidx.lifecycle.MutableLiveData;

import com.rany.mvvmdemo.http.bean.DialogBean;


/**
 * Description：用于在basemoudel 统一控制loading
 * Created on 2020/6/9
 * Author : 郭
 */
public final class DialogLiveData<T> extends MutableLiveData<T> {

    private DialogBean bean = new DialogBean();

    public void setValue(boolean isShow) {
        bean.setShow(isShow);
        bean.setMsg("");
        setValue((T) bean);
    }

    public void setValue(boolean isShow, String msg) {
        bean.setShow(isShow);
        bean.setMsg(msg);
        setValue((T) bean);
    }
}