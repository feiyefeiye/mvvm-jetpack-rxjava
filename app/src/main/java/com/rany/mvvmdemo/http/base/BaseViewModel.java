package com.rany.mvvmdemo.http.base;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.OnLifecycleEvent;

import com.rany.mvvmdemo.bean.TitleBean;
import com.rany.mvvmdemo.http.bean.DialogBean;
import com.rany.mvvmdemo.http.livedata.DialogLiveData;
import com.rany.mvvmdemo.http.livedata.TitleLiveData;
import com.rany.mvvmdemo.http.manager.ApiManager;
import com.rany.mvvmdemo.http.manager.RetrofitManager;
import com.rany.mvvmdemo.http.manager.SubscriberManger;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Description：base基类，所有的viewmoudel均需继承
 * Created on 2020/9/15
 * Author : 郭
 */
public abstract class BaseViewModel extends AndroidViewModel implements LifecycleObserver {

    /**
     * 管理RxJava请求
     */
    private CompositeDisposable compositeDisposable;
    /**
     * 用来通知 Activity／Fragment 是否显示等待Dialog
     */
    protected DialogLiveData<DialogBean> showDialog = new DialogLiveData<>();
    /**
     * 当ViewModel层出现错误需要通知到Activity／Fragment
     */
    protected MutableLiveData<Object> error = new MutableLiveData<>();
    /**
     * 设置title
     */
    public TitleLiveData<TitleBean> title = new TitleLiveData<>();

    /**
     * retrofit
     */
    public ApiManager manager = RetrofitManager.getInstance().createApiManager();


    public BaseViewModel(@NonNull Application application) {
        super(application);
    }

    /**
     * 获取请求retrofit
     *
     * @return
     */
    public ApiManager RetrofitManager() {
        if (manager == null) {
            return RetrofitManager.getInstance().createApiManager();
        } else {
            return manager;
        }
    }

    /**
     * 添加 rxJava 发出的请求
     *
     * @param observable
     * @param observer
     */
    public void addDisposable(Observable<?> observable, SubscriberManger observer) {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer));
    }

    /**
     * loading框控制
     *
     * @param owner
     * @param observer
     */
    public void getShowDialog(LifecycleOwner owner, Observer<DialogBean> observer) {
        showDialog.observe(owner, observer);
    }

    /**
     * 接口报错
     *
     * @param owner
     * @param observer
     */
    public void getError(LifecycleOwner owner, Observer<Object> observer) {
        error.observe(owner, observer);
    }


    /**
     * ViewModel销毁同时也取消请求
     */
    @Override
    protected void onCleared() {
        super.onCleared();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
        showDialog = null;
        error = null;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected void onCreate() {
        setTitle();
    }

    public abstract void setTitle();

    protected void onLoadMore() {

    }




}
