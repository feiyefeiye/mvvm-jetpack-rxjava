package com.rany.mvvmdemo.http.loading;


import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.rany.mvvmdemo.R;
import com.rany.mvvmdemo.databinding.DialogLoadingBinding;
import com.rany.mvvmdemo.utils.AppUtil;

/**
 * Description：loading 用于请求
 * Created on 2020/6/8
 * Author : 郭
 */
public class LoadingDialog extends Dialog {

    private DialogLoadingBinding binding;


    public LoadingDialog(@NonNull Context context) {
        super(context, R.style.LoadingDialog);
        setCanceledOnTouchOutside(false);
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_loading, null, false);
        setContentView(binding.getRoot());
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = AppUtil.dpToPx(context, 150);
        lp.height = AppUtil.dpToPx(context, 110);
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);
    }

    /**
     * 设置等待提示信息
     */
    public void setLoadingMsg(String msg) {
        if (TextUtils.isEmpty(msg)) {
            return;
        }
        binding.tvMsg.setText(msg);
    }
}
