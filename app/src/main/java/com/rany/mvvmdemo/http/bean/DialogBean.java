package com.rany.mvvmdemo.http.bean;

/**
 * Description：dialog封装实体 用于显示 隐藏请求loading
 * Created on 2020/6/8
 * Author : 郭
 */
public final class DialogBean {

    private boolean isShow;
    private String msg;

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "DialogBean{" +
                "isShow=" + isShow +
                ", msg='" + msg + '\'' +
                '}';
    }
}
