package com.rany.mvvmdemo.http.livedata;

import androidx.lifecycle.MutableLiveData;

import com.rany.mvvmdemo.bean.TitleBean;

/**
 * Description：title封装 在basemoudel统一控制
 * Created on 2020/6/9
 * Author : 郭
 */
public final class TitleLiveData<T> extends MutableLiveData<T> {

    private TitleBean bean = new TitleBean();

    /**
     * 展示title
     * @param showLeft true 展示返回
     * @param title
     */
    public void setTitleBar(boolean showLeft, String title) {
        bean.setTitleLeft(showLeft);
        bean.setTitle(title);
        setValue((T) bean);
    }

    public void setTitleBar(boolean showLeft, String title, String titleRight) {
        bean.setTitleLeft(showLeft);
        bean.setTitleRight(titleRight);
        bean.setTitle(title);
        setValue((T) bean);
    }

    public void setTitleBar(boolean showLeft, String title, int titleRight) {
        bean.setTitleLeft(showLeft);
        bean.setTitleRightRes(titleRight);
        bean.setTitle(title);
        setValue((T) bean);
    }
}
